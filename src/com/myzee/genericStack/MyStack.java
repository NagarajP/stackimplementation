package com.myzee.genericStack;

import java.util.Arrays;

public class MyStack {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		CustomStack st = new CustomStack(4);
		st.print();
		st.push(1);
		st.push(2);
		st.push(3);
		st.push(4);
		st.push(5);
		System.out.println();
		st.print();
		
		System.out.println("-------");
		System.out.println("popped item - " + st.pop());
		System.out.println("popped item - " + st.pop());
		
		st.print();
	}

}

class CustomStack {
	private int[] stack;
	private int maxSize;
	private int top = -1;
	
	public CustomStack(int maxSize) {
		super();
		this.maxSize = maxSize;
		this.stack = new int[this.maxSize];
	}
	
	public void push(int element) {
		if(isFull()) {
			System.out.print("Stack is Full!!");
		} else {
			this.stack[++top] = element;
		}
	}
	
	public int pop() {
		if(isEmpty()) {
			System.out.println("Stack Empty!!");
			return -1;
		} else {
			return stack[this.top--];
		}
	}
	
	public boolean isFull() {
		if(this.top == this.maxSize - 1 ) {
			return true;
		}
		return false;
	}
	private boolean isEmpty() {
		if(this.top == -1) {
			return true;
		} 
		return false;
	}
	
	public void print() {
		if(isEmpty()) {
			System.out.println("Stack Empty!! cannot print");
		} else {
//			Arrays.stream(stack).forEach(System.out::println);
			for(int i = top; i >= 0; i--) {
				System.out.println(stack[i]);
			}
		}
	}
	
}