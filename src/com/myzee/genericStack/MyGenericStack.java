package com.myzee.genericStack;

import java.util.Arrays;
import java.util.Stack;

public class MyGenericStack {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("Stack of Integers\n-----------------------------------------");
		StackE<Integer> st = new StackE<Integer>(4);
//		st.print();
		st.push(1);
		st.push(2);
		st.push(3);
		st.push(4);
//		st.push(5);	//stack is full condition

		st.print();
		
		st.pop();
		st.pop();
		
		st.print();
		
		System.out.println("\nStack of Strings\n-----------------------------------------------");
		StackE<String> s = new StackE<String>(4);
//		s.print();
		s.push("List");
		s.push("Set");
		s.push("Map");
		s.print();
		s.pop();
		s.print();
		
		System.out.println("\nStack of Worker Objects\n-------------------------------------------------");
		StackE<Worker> sw = new StackE<Worker>(4);
		sw.push(new Worker(1, "Bob"));
		sw.push(new Worker(2, "Cherry"));
		sw.push(new Worker(3, "Ketherien"));
		sw.push(new Worker(4, "Mark"));
		sw.print();
		
	}

}

class StackE<T> {
	private Object[] stack;
	private int maxSize;
	private int top = -1;
	
	public StackE(int maxSize) {
		super();
		this.maxSize = maxSize;
		this.stack = new Object[this.maxSize];
	}
	
	public <E> void push(E element) {
		if(isFull()) {
			System.out.print("Stack is Full!!");
		} else {
			this.stack[++top] = element;
		}
	}
	
	public void pop() {
		if(isEmpty()) {
			System.out.println("Stack Empty!!");
		} else {
			System.out.println("Item popped is =\t\t " + stack[this.top--]);
		}
	}
	
	public boolean isFull() {
		if(this.top == this.maxSize - 1 ) return true;
		return false;
		
	}
	private boolean isEmpty() {
		if(this.top == -1) return true;
		return false;
	}
	
	public void print() {
		if(isEmpty()) {
			System.out.println("Stack Empty!! cannot print");
		} else {
//			Arrays.stream(stack).forEach(System.out::println);
			for(int i = top; i >= 0; i--) {
				System.out.println(stack[i]);
			}
		}
	}
	
}

class Worker {
	private int id;
	private String name;
	public Worker(int id, String name) {
		super();
		this.id = id;
		this.name = name;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Worker [id=" + id + ", name=" + name + "]";
	}
	
	
}